import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cards',
  templateUrl: './cards.component.html',
  styleUrls: ['./cards.component.scss']
})
export class CardsComponent implements OnInit {

  loadingOverlay:string;
  constructor() { }

  ngOnInit(): void {
    setTimeout(() => {
      this.loadingOverlay ="hidden";
    }, 3000);
  }
}
